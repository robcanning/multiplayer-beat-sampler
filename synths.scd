//////////////////////////////////////////////////////////

SynthDef(\bpfbuf, {
	arg atk=0, sus=0, rel=0.5, c1=1, c2=(-1),
	buf=1, rate=1, spos=0, sposrand=11110, freq=440, rq=1, bpfmix=1,
	pan=0, panwidth=1, amp=1, out=0;
	var sig=0, env;
	env = EnvGen.kr(Env([0,1,1,0],[atk,sus,rel],[c1,0,c2]),doneAction:2);
	sig = PlayBuf.ar(1, buf, rate*BufRateScale.ir(buf),
		startPos:spos - LFNoise0.kr(0.01).range(0,sposrand)); //random offset of start position
	sig = XFade2.ar(sig, BPF.ar(sig, freq, rq, 1/rq.sqrt), bpfmix*2-1);
	sig = sig * env;
	sig = Pan2.ar(sig, pan*panwidth, amp);

	Out.ar(out, sig);
}).add;

"SYNTHS loaded".postln;

////////////////////////////////////////////////////////

SynthDef(\kick, {
	arg freq=60, envfrq, atk=0.005, rel=0.08, cur=4, out=0, amp=0;
	var env, sig;
	env = Env.perc(atk, rel, 1, cur).kr(2);
	envfrq = Env([300,60,50], [atk,rel]).kr(2); //envelope the pitc$
	sig = SinOsc.ar(envfrq, mul:env)!2;
	Out.ar(out, sig * amp);
}).add;

SynthDef(\snare, {
	arg freq=60,  atk=0.01, rel=0.3, cur=4, amp=0.1, hpfreq=2000, out=0;
	var env, sig, ring, sum=0;
	env = Env.perc(atk, rel, amp, cur).kr(2);
	sig = WhiteNoise.ar(freq,  mul:env)!2;
	sig = HPF.ar(sig, hpfreq)!2;
	ring = SinOsc.ar(freq, mul:env)!2;
	//sum = sig + ring;
	sum = sum * amp;
	//sig = Mix.ar(2, [sig, sig]);
	Out.ar(out, sum);
}).add;

SynthDef(\noise, {
	arg amp=0.5, atk=0.01, rel=0.2, ffreq=6000, pan=0, out=0;
	var env, snd;
	env = Env([0,1,0], [atk,rel], 'lin').kr(2);
	snd = WhiteNoise.ar;
	snd = HPF.ar(in: snd, freq: ffreq,  mul: env);
	snd = Pan2.ar(snd, pan, amp);
	Out.ar(out, snd);
}).add;

SynthDef(\noiselive, {
	arg amp=0.5, atk=0.01, rel=0.2, gate=1, ffreq=6000, rq=1, pan=0, out=0;
	var env, snd;
	env = Env.adsr(atk, 1, 1, rel, 1, -4).kr(2, gate:gate);
	snd = WhiteNoise.ar;
	snd = BPF.ar(in: snd, freq: ffreq, rq:rq, mul: env);
	snd = Pan2.ar(snd, pan, amp);
	Out.ar(out, snd);
}).add;

// put an ASDR
SynthDef(\sinwave, {
	arg out=0, freq=440, amp=0,
	pan=0,  pfreq=0.1, pphase=0,
	atk=0.1,sus=0.1,rel=0.1;
	var tmp=0, sig=0, env, posc;
//	env = Env([0,1,0], [atk,rel], 'lin').kr(2);
	env = Env.perc(attackTime:atk, releaseTime:rel,curve:-4).kr(2);

	2.do({
		tmp = SinOsc.ar(freq + Rand(0,6));
		//pan = SinOsc.kr(pfreq pphase);
		tmp = Pan2.ar(tmp, pan); //width -- add as arg
		sig = sig + tmp;
	});
	sig = sig * env;
	sig=sig*amp;
	Out.ar(out, sig);
}).add;

SynthDef(\sawwave, {
	arg out=0, freq=440, amp=0,
	pan=0,  pfreq=0.1, pphase=0,
	atk=0.5,sus=0.5,rel=0.5;
	var tmp=0, sig=0, env, posc;
//	env = Env([0,1,0], [atk,rel], 'lin').kr(2);
	env = Env.perc(attackTime:atk, releaseTime:rel,curve:-4).kr(2);

	2.do({
		tmp = LFSaw.ar(freq + Rand(0,3));
		//pan = SinOsc.kr(pfreq pphase);
		tmp = Pan2.ar(tmp, pan); //width -- add as arg
		sig = sig + tmp;
	});
	sig = sig * env;
	sig=sig*amp*0.7;
	Out.ar(out, sig);
}).add;

SynthDef(\triwave, {
	arg out=0, freq=440, amp=0,
	pan=0,  pfreq=0.1, pphase=0,
	atk=0.5,sus=0.5,rel=0.5;
	var tmp=0, sig=0, env, posc;
//	env = Env([0,1,0], [atk,rel], 'lin').kr(2);
	env = Env.perc(attackTime:atk, releaseTime:rel,curve:-4).kr(2);

	2.do({
		tmp = LFTri.ar(freq + Rand(0,6));
		//pan = SinOsc.kr(pfreq pphase);
		tmp = Pan2.ar(tmp, pan); //width -- add as arg
		sig = sig + tmp;
	});
	sig = sig * env;
	sig=sig*amp;
	Out.ar(out, sig);
}).add;

SynthDef(\playbuf, {
	arg atk=0, sus=0, rel=3, c1=1, c2=(-1),
	buf=1, rate=1, spos=0, gate=1,
	pan=0, panwidth=1, amp=1, out=0;
	var sig=0, env;
	env = Env.adsr(atk, 1, 1, rel, 1, -4).kr(2, gate:gate);
	sig = PlayBuf.ar(1, buf, rate*BufRateScale.ir(buf), startPos:0); //random offset of start position
	sig = sig * env;
	//sig = XFade2.ar(sig, BPF.ar(sig, 10000, 1, 1/1.sqrt), 0*2-1);
	sig = Pan2.ar(sig, pos:0, level:amp);
	Out.ar(out, sig);
}).add;

 SynthDef(\granularOne, {
	arg out=0,buf=c,durx=12,trate=10, posx=1,posxfine=1,rndoff=0,freq=1,gain=0, atk=0, rel=3, gate=1;
    var dur, clk, pos, pan, env, sig;
	dur = durx / trate;
	clk = WhiteNoise.kr(1);//Impulse.kr(trate*rrand(0.5,1.2));
	pos = BufDur.kr(buf) * (posx/127)+posxfine + TRand.kr(0, rndoff, clk);
    pan = WhiteNoise.kr(0.6);

	env = Env.adsr(atk, 1, 1, rel, 1, -4).kr(2, gate:gate);
    sig = TGrains.ar(2, clk, buf, freq, pos, dur, pan, 0.1);
	sig = sig*env;
	Out.ar(out,sig*gain);
	}).add;

SynthDef(\delay, {
	arg in, deltime=0.22, delfb=0.7,
	amp=1, out=0, buf=~delb.bufnum;
	var dry, wet, temp, sig;
	dry = In.ar(in,2);
	sig = PingPong.ar(buf,dry, deltime, delfb,);
	sig = sig + dry;
	//sig = Compander.ar (in: sig, control: 0, thresh: 0.5, slopeBelow: 1, slopeAbove: 1, clampTime: 0.01, relaxTime: 0.1, mul: 1, add: 0);
	Out.ar(out, sig);
}).add;

SynthDef(\reverb, {
	arg in, predelay=0.1, revtime=0.8,
	lpf=4500, mix=0.15, amp=1, out=0;
	var dry, wet, temp, sig;
	dry = In.ar(in,2);
	temp = In.ar(in,2);
	wet = 0;
	temp = DelayN.ar(temp, 0,2, predelay);
	16.do{
		temp = AllpassN.ar(temp, 0.05, {Rand(0.001,0.05)}!2, revtime);
		temp = LPF.ar(temp, lpf);
		wet = wet + temp;
	};
	sig = XFade2.ar(dry, wet, mix*2-1, amp);
	Out.ar(out, sig);
}).add;

SynthDef(\ringMod, {
	arg in, amp=1, out=0,freq=440, rmodmix=1, mode=1,rmmode;
	var  sig, mod, rand, sine;
	/*switch (1,
	0, {rmmode = LFNoise0.kr(~tempo*10).exprange(150, 10000);},
	1, {rmmode = SinOsc.kr(0.1).exprange(150, 1000);}
	);*/
	rmmode = LFNoise0.kr(~tempo*10).exprange(150, 10000);
	mod = SinOsc.ar(rmmode);
	mod = mod * rmodmix/2 * amp;
	sig = In.ar(in,2)!2;
	sig = mod*sig;
	//SinOsc.ar(LFNoise0.ar(12).exprange(250, 1000));
	Out.ar(out, sig);
}).add;